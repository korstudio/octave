//
//  AuthenticationService.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias AuthenticationCallback = (String) -> ()

protocol AuthenticationServiceProtocol {
    func requestToken(username: String, password: String, callback: @escaping AuthenticationCallback)
}

class AuthenticationService: NSObject {
    var callback: AuthenticationCallback?
    
    override init() {
        super.init()
    }
}

extension AuthenticationService: AuthenticationServiceProtocol {
    func requestToken(username: String, password: String, callback: @escaping AuthenticationCallback) {
        self.callback = callback
        
        let bundle = APIEndpoint.getBundle()
        
        let requestURL = URL(string: "\(bundle["Base URL"] ?? "")\(bundle["Access Token Endpoint"] ?? "")")
        
        guard let _url = requestURL else {
            print("requestURL is empty")
            return
        }
        
        let params = ["username": username, "password": password, "grant_type": "password"]
        
        Alamofire.request(_url, method: .post, parameters: params).responseJSON {
            response in
            response.result.ifSuccess {
                if let responseValue = response.result.value {
                    let responseJson = JSON(responseValue)
                    callback(responseJson["access_token"].stringValue)
                } else {
                    callback("")
                }
            }
        }
    }

    
}
