//
//  SurveyService.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias SurveyServiceCallback = ([SurveyItem]) -> ()

protocol SurveyServiceProtocol {
    func getSurveysPerPage(accessToken: String, page: Int, perPage: Int, callback: @escaping SurveyServiceCallback)
    func getSurveys(accessToken: String, callback: @escaping SurveyServiceCallback)
}

class SurveyService: NSObject {
    var callback: SurveyServiceCallback? = nil
    
    override init() {
        super.init()
    }
}

extension SurveyService: SurveyServiceProtocol {
    func getSurveysPerPage(accessToken: String, page: Int, perPage: Int, callback: @escaping SurveyServiceCallback) {
        self.callback = callback
    }

    
    func getSurveys(accessToken: String, callback: @escaping SurveyServiceCallback) {
        self.callback = callback

        let bundle = APIEndpoint.getBundle()
        
        let requestURL = URL(string: "\(bundle["Base URL"] ?? "")\(bundle["Get Surveys Endpoint"] ?? "")?access_token=\(accessToken)")
        
        guard let _url = requestURL else {
            print("requestURL is empty")
            return
        }
        
        Alamofire.request(_url, method: .get).responseJSON {
            response in
            response.result.ifSuccess {
                if let responseValue = response.result.value {
                    let responseJson = JSON(responseValue)
                    let surveyResponse = SurveyParser.parse(data: responseJson)
                    callback(surveyResponse)
                } else {
                    callback([])
                }
            }
        }
    }

    
}
