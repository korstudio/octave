//
//  ViewController.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/13/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import UIKit
import PKHUD
import AlamofireImage

class MainViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var viewModel: MainViewModel!
    var surveyItems: [SurveyItem]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initViewModel()
        loadSurveys()
    }
    
    func loadSurveys() {
        self.viewModel.authenticateUserAndLoadSurveys(username: "carlos@nimbl3.com", password: "antikera", fail: {
            print("Cannot authenticate user")
        }) { (surveyItems: [SurveyItem]) in
            self.surveyItems = surveyItems
            
            self.renderSurveys()
        }
    }
    
    @IBAction func onRefreshClick(_ sender: Any) {
        loadSurveys()
    }
}

extension MainViewController: MainViewProtocol {
    func initViewModel() {
        self.viewModel = MainViewModel(view: self, surveyService: SurveyService(), authenService: AuthenticationService())
    }
    
    func showProgress() {
        HUD.show(.labeledProgress(title: "Loading...", subtitle: nil))
    }
    
    func hideProgress() {
        HUD.hide()
    }
    
    func openSurvey() {
        
    }
}

// MARK: custom view functions
extension MainViewController: UIScrollViewDelegate {
    
    func initView() {
        self.scrollView.delegate = self
        self.pageControl.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 0.5)
    }
    
    func renderSurveys() {
        guard let surveyItems = self.surveyItems else {
            return
        }
        
        removeScrollViewSubviews()
        
        self.scrollView.isPagingEnabled = true
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: self.scrollView.frame.height * CGFloat(surveyItems.count))
        
        for (index, item) in surveyItems.enumerated() {
            let view = self.createSurveyPlane(index: index, item: item)
            
            self.scrollView.addSubview(view)
        }
        
        self.pageControl.numberOfPages = surveyItems.count
    }
    
    func createSurveyPlane(index: Int, item: SurveyItem) -> UIView {
        let plane = UIView(frame: CGRect(x: 0, y: CGFloat(index) * self.scrollView.frame.height, width: self.scrollView.frame.width, height: self.scrollView.frame.height))
        
        let placeholderImage = createPlaceholderImage()
        
        let imageURL = URL(string: "\(item.coverImageUrl)l")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height))
        imageView.af_setImage(withURL: imageURL, placeholderImage: placeholderImage)
        imageView.contentMode = .scaleAspectFill
        
        let textView = UILabel(frame: CGRect(x: 16.0, y: 64.0, width: self.scrollView.frame.width - 32.0, height: 100.0))
        textView.text = item.title
        textView.font = UIFont(name: "AvenirNext-Bold", size: 32.0)
        textView.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        textView.textAlignment = .center
        textView.numberOfLines = 0
        
        let descTextLabel = UILabel(frame: CGRect(x: 16.0, y: 140.0, width: self.scrollView.frame.width - 32.0, height: 120.0))
        descTextLabel.text = item.desc
        descTextLabel.font = UIFont(name: "AvenirNext-Light", size: 17.0)
        descTextLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        descTextLabel.textAlignment = .center
        descTextLabel.numberOfLines = 0
        
        let button = UIButton(frame: CGRect(x: 16.0, y: self.scrollView.frame.height - 64.0, width: self.scrollView.frame.width - 32.0, height: 48.0))
        button.setTitle("Take Survey", for: .normal)
        button.backgroundColor = UIColor(red: 181.0/255.0, green: 72.0/255.0, blue: 139.0/255.0, alpha: 1)
        button.tag = 100 + index
        button.addTarget(self, action: #selector(onOpenSurveyButtonClick(sender:)), for: .touchUpInside)
        
        plane.addSubview(imageView)
        plane.addSubview(textView)
        plane.addSubview(descTextLabel)
        plane.addSubview(button)
        
        return plane
    }
    
    func createPlaceholderImage() -> UIImage! {
        let color = UIColor(red: 53.0/255.0, green: 59.0/255.0, blue: 68.0/255.0, alpha: 1)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.scrollView.frame.width, height: self.scrollView.frame.height))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else {
            return nil
        }
        return UIImage(cgImage: cgImage)
    }
    
    func removeScrollViewSubviews() {
        if self.scrollView.subviews.count > 0 {
            for view in self.scrollView.subviews {
                view.removeFromSuperview()
            }
        }
    }
    
    func onOpenSurveyButtonClick(sender: UIButton) {
        performSegue(withIdentifier: "OpenSurveySegue", sender: sender)
    }
    
    // MARK: scroll view delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageIndex = scrollView.contentOffset.y / scrollView.frame.height
        self.pageControl.currentPage = Int(pageIndex)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OpenSurveySegue" {
            if let detailViewController = segue.destination as? SurveyDetailViewController {
                if let button = sender as? UIButton {
                    let itemIndex = button.tag - 100
                    detailViewController.setSurveyItem(surveyItem: self.surveyItems[itemIndex])
                }
            }
        }
    }
}
