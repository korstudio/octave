//
//  SurveyDetailViewController.swift
//  Octave
//
//  Created by korstudio on 9/15/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import Foundation
import UIKit

class SurveyDetailViewController: UIViewController {
    
    @IBOutlet weak var surveyTitleLabel: UILabel!
    
    var surveyItem: SurveyItem! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    func setSurveyItem(surveyItem: SurveyItem) {
        self.surveyItem = surveyItem
    }
    
    func initView() {
        self.surveyTitleLabel.text = self.surveyItem.title
    }
    
    @IBAction func onFinishClick(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
