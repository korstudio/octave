//
//  ThemeItem.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ThemeItem: BaseModel {
    var colorActive: String = ""
    var colorInactive: String = ""
    var colorQuestion: String = ""
    var colorAnswerNormal: String = ""
    var colorAnswerInactive: String = ""
    
    mutating func parse(data: JSON) -> ThemeItem {
        self.colorActive = data["color_active"].stringValue
        self.colorInactive = data["color_inactive"].stringValue
        self.colorQuestion = data["color_question"].stringValue
        self.colorAnswerNormal = data["color_answer_normal"].stringValue
        self.colorAnswerInactive = data["color_answer_inactive"].stringValue
        return self
    }
}
