//
//  QuestionItem.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation
import SwiftyJSON

struct QuestionItem: BaseModel {
    var id: String = ""
    var text: String = ""
    var displayType: String = ""
    var coverImageUrl: String = ""
    var answers: [AnswerItem] = []
    
    mutating func parse(data: JSON) -> QuestionItem {
        self.id = data["id"].stringValue
        self.text = data["text"].stringValue
        self.displayType = data["display_type"].stringValue
        self.coverImageUrl = data["cover_image_url"].stringValue
        
        for answerJson: JSON in data["answers"].arrayValue {
            var answerItem = AnswerItem()
            _ = answerItem.parse(data: answerJson)
            self.answers.append(answerItem)
        }
        
        return self
    }
}

struct AnswerItem: BaseModel {
    var id: String = ""
    var questionId: String = ""
    var text: String = ""
    
    mutating func parse(data: JSON) -> AnswerItem {
        self.id = data["id"].stringValue
        self.questionId = data["question_id"].stringValue
        self.text = data["text"].stringValue
        
        return self
    }
}
