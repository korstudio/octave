//
//  SurveyItem.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SurveyItem: BaseModel {
    var id: String = ""
    var title: String = ""
    var desc: String = ""
    var thankEmailAboveThreshold: String = ""
    var thankEmailBelowThreshold: String = ""
    var footerContent: String = ""
    var isActive: Bool = true
    var coverImageUrl: String = ""
    var theme: ThemeItem = ThemeItem()
    var questions: [QuestionItem] = []
    
    mutating func parse(data: JSON) -> SurveyItem {
        self.id = data["id"].stringValue
        self.title = data["title"].stringValue
        self.desc = data["description"].stringValue
        self.thankEmailAboveThreshold = data["thank_email_above_threshold"].stringValue
        self.thankEmailBelowThreshold = data["thank_email_below_threshold"].stringValue
        self.footerContent = data["footer_content"].stringValue
        self.isActive = data["is_active"].boolValue
        self.coverImageUrl = data["cover_image_url"].stringValue
        _ = self.theme.parse(data: data["theme"])
        
        for questionObj: JSON in data["questions"].arrayValue {
            var q = QuestionItem()
            _ = q.parse(data: questionObj)
            self.questions.append(q)
        }
        
        return self
    }
}
