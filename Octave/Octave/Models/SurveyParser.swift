//
//  SurveyParser.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation
import SwiftyJSON

class SurveyParser: NSObject {
    static func parse(data: JSON) -> [SurveyItem] {
        var surveyList = [SurveyItem]()
        
        for (_, surveyDict):(String, JSON) in data {
            var surveyItem = SurveyItem()
            _ = surveyItem.parse(data: surveyDict)
            surveyList.append(surveyItem)
        }
        
        return surveyList
    }
}
