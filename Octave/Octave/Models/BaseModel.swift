//
//  BaseModel.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol BaseModel {
    mutating func parse(data: JSON) -> Self
}
