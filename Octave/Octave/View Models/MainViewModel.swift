//
//  MainViewModel.swift
//  Octave
//
//  Created by korstudio on 9/14/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import Foundation

class MainViewModel: NSObject {
    
    var view: MainViewProtocol!
    var surveyService: SurveyService!
    var authenService: AuthenticationService!
    
    var accessToken: String!
    
    init(view: MainViewProtocol, surveyService: SurveyService, authenService: AuthenticationService) {
        super.init()
        
        self.view = view
        self.surveyService = surveyService
        self.authenService = authenService
    }
    
    func authenticateUser(username: String, password: String, fail failCallback: @escaping ()->(), success successCallback: @escaping (String)->()) {
        self.view.showProgress()
        self.authenService.requestToken(username: username, password: password) { 
            accessToken in
            
            self.view.hideProgress()
            
            self.accessToken = accessToken
            UserDefaults.standard.set(accessToken, forKey: "accessToken")
            
            successCallback(accessToken)
        }
    }
    
    func loadSurveys(fail failCallback: ()->(), success successCallback: @escaping ([SurveyItem])->()) {
        self.view.showProgress()
        
        guard let token = self.accessToken else {
            self.view.hideProgress()
            return
        }
        
        self.surveyService.getSurveys(accessToken: token) { (surveyItems: [SurveyItem]) in
            self.view.hideProgress()
            
            successCallback(surveyItems)
        }
    }
    
    func authenticateUserAndLoadSurveys(username: String, password: String, fail failCallback: @escaping ()->(), success successCallback: @escaping ([SurveyItem])->()) {
        
        self.authenticateUser(username: username, password: password, fail: {
            self.view.hideProgress()
            failCallback()
        }) { (accessToken: String) in
            self.loadSurveys(fail: failCallback, success: { (surveyItems: [SurveyItem]) in
                self.view.hideProgress()
                successCallback(surveyItems)
            })
        }
    }
    
    func getAccessTokenFromLocal() -> String {
        return UserDefaults.standard.string(forKey: "accessToken") ?? ""
    }
}
