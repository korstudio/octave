//
//  MainViewProtocol.swift
//  Octave
//
//  Created by korstudio on 9/14/17.
//  Copyright © 2017 Methas Tariya. All rights reserved.
//

import Foundation

protocol MainViewProtocol {
    func showProgress()
    func hideProgress()
    func openSurvey()
}
