//
//  APIEndpoint.swift
//  Octave
//
//  Created by LSWMacBookPro on 9/14/2560 BE.
//  Copyright © 2560 Methas Tariya. All rights reserved.
//

import Foundation

class APIEndpoint: NSObject {
    
    static func getBundle() -> [String: String] {
        let path = Bundle.main.path(forResource: "API", ofType: "plist")
        
        guard let _path = path else {
            return [String: String]()
        }
        
        let bundleDict = NSDictionary(contentsOfFile: _path) as? [String : String]
        guard let _bundleDict = bundleDict else {
            return [String: String]()
        }
        
        return _bundleDict
    }
    
}
